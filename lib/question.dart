import 'package:flutter/material.dart';

// Question does not have an underscore since we want to be able to use the question widget
// in the main dart file
class Question extends StatelessWidget {
  // Initialize property
  // User final to enforce that the value of questionText will not
  // change after its initialization in the constructor
  // questionText stores the value passed when the Question widget is called in the main widget
  final String questionText;

  // Initialize constructor, pass questionText as a positional argument
  Question(this.questionText);

  @override
  Widget build(BuildContext context) {
    // Access questionText property set in the constructor
    return Container(
      // Set size of the container to be max width of the device
      width: double.infinity,

      // Check docs
      margin: EdgeInsets.all(10),
      child: Text(
        questionText,
        style: TextStyle(fontSize: 28),
        textAlign: TextAlign.center,
      ),
    );
  }
}
