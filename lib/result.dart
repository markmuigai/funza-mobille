import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  // Initialize properties
  final int resultScore;
  final Function resetHandler;
  final int maxScore;

  // Constructor
  Result(this.resultScore, this.resetHandler, this.maxScore);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            'Your compatibility with the app is $resultScore out of $maxScore',
            style: TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            onPressed: resetHandler,
            child: Text(
              'Restart Quiz',
            ),
            textColor: Colors.blue,
          )
        ],
      ),
    );
  }
}
