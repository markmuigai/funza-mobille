import 'package:flutter/material.dart';

// Import question widget from external file
import 'quiz.dart';
import 'result.dart';

void main() => runApp(MyApp());

// In a STATELESS widget, we have widget and a build method which is used to render the UI
// We can pass data from outside into the stateless widget throught the constructor pf the widget class
// Flutter will rebuild the widget when the external data changes
// Data will never change inside of the widget class, ew can only receive data from outside that will rebuild the widget
// A STATEFUL WIDGET unlike a stateless widget, has an internal state, which can also cause the widget UI to be rerendered

// A stateful widget is a combination of 2 classes. Why?
// The MyApp class can be recreated, but the state MyAppState is persistent.
// My AppState is persitent so that the state data is not reset
// How do we tell flutter that a state class belongs to a widget class?
// 2 things to set a connection between the 2
// In AppState class: since state is a generic type, thus use <>, then  add pointer to MyApp ,
// In MyApp widget class: Add new createStateMethod, that takes no arguments but returns a state object
// that is connected to a stateful widget i.e MyAppState
// The build method is now inside the state and not inside the widget
// because the state holds the data used by the build method
class MyApp extends StatefulWidget {
  // Override createState method in stateful widget class
  @override

  // Return a state object
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

// Named after widget name and State after it
// State is a generic class imported from material dart
// Add a leading underscore to class name to enforce that the class should only be usable from inside the main dart file, i.e
// from inside the file from which its defined
// The underscore turns the class from a public to a private class
class _MyAppState extends State<MyApp> {
  // Initialize a list that stores maps with a <string, list> key value pair,
  // const vs final
  // FInal is runtime constant while const is also compile time constant
  // Final properties are changed initially when values are assigned in the constructor
  // At runtime, the final value gets locked in, but during development, the value is not set
  // e.g during developing, value passed to question is based on the instance created
  // The questions list, however, is never changed thus it is always constant, constant
  // during both development and compilation
  final _questions = const [
    {
      'questionText': 'Where did you hear about us?',
      'answers': [
        {'text': 'Social Media', 'score': 5},
        {'text': 'Word of Mouth', 'score': 3},
        {'text': 'By Chance', 'score': 1},
      ],
    },
    {
      'questionText': 'Which level of learning are you interested in?',
      'answers': [
        {'text': 'Primary', 'score': 5},
        {'text': 'Secondary', 'score': 4},
        {'text': 'University', 'score': 3},
        {'text': 'Post Gradueate', 'score': 2}
      ],
    },
    {
      'questionText': 'What\'s your capacity?',
      'answers': [
        {'text': 'Teacher', 'score': 5},
        {'text': 'Student', 'score': 2},
        {'text': 'School owner', 'score': 5},
        {'text': 'Education Official', 'score': 5}
      ],
    }
  ];

  // Getter
  int get maxScore {
    return _questions.map((question) {
        return (question['answers'] as List<Map<String, Object>>).map((answer) {
          return answer['score'];
        }).toList();
      })
      .toList()
      .expand((i) => i)
      .toList()
      .fold(0, (p, c) => p + c);
  }

  // Add underscore to class properties on initialization and wherever they are referenced to enforce that it is a private property,
  // Initialize loop counter
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    // UPdate values in set state so that build method rebuilds the entire widget tree
    // This re evaluating the index < length condition which will pass thus not
    // rendering the result of the quiz

    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  // Add a method
  void _answerQuestion(int score) {
    _totalScore += score;

    // Method provided by State class that takes an anonymous function as an argument
    // with expressions that change properties used in the state
    // Set state rerenders the UI by calling the build method of the widget wehere you call set state,
    // thus rerenders only a portion of the UI instead of the entire UI
    setState(() {
      // Increment question counter every time the function is called
      _questionIndex++;
    });

    print(_questionIndex);
  }

  // Override build method in Stateless widget class
  @override

  // Is executed every time flutter rebuild the app interface
  Widget build(BuildContext context) {
    print(maxScore);
    //  Overall app widget Material App
    // Uses Scaffold widget to get some basic styling
    return MaterialApp(
        home: Scaffold(
      // Instance of Appbar widget class
      // Pass a text widget to the title
      appBar: AppBar(
        title: Text('Funza App'),
      ),

      // Body is a named argument, can only take one widget
      // Column widget that renders widgets in a column
      // Children named argument that takes a list of widgets
      // if there are more questions if else shortcode
      // else return text in a center widget
      body: _questionIndex < _questions.length
          ? Quiz(
              answerQuestion: _answerQuestion,
              questionIndex: _questionIndex,
              questions: _questions)
          : Result(_totalScore, _resetQuiz, maxScore),
    ));
  }
}
