import 'package:flutter/material.dart';
import 'question.dart';
import 'answer.dart';

class Quiz extends StatelessWidget {
  // Initialize a questions list that stores maps with a <string, list> key value pair
  final List<Map<String, Object>> questions;
  final int questionIndex;
  final Function answerQuestion;

  // Initialize constructor
  // Pass list to enforce passing arguments as named values when they are called
  // Add aquired decorator provided by dart to ensure all arguments are passed
  Quiz({
    @required this.questions, 
    @required this.answerQuestion, 
    @required this.questionIndex}
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // Access items in question list
        // Text widget has no state, i.e it does not react to anything on the app
        // Spltting widgets to indivudial files improves performance by making rebuilds more effecient
        // and having cleaner code
        Question(
          // First index passed to select the map
          // Second to select the key value pair of the selected map i.e
          // access value with the key questionText
          questions[questionIndex]['questionText']
        ),
        // Just as you can pass text to a widget, you can also pass the pointer at a function to a widget
        // pass _answer question without parenthesis to call the pointer instead of executing the function
        // We will need to accept answeQuestion as an argument to the constructor  
        // Answer(_answerQuestion),
        
        // Transform the list of maps (questions) to a list of widgets that generate the answer
        // Use anonymous function

        // Enclose in brackets and identify the values of answers key as a list of strings 
        // toList converts the return values of map to a list 
        // The ... operator take a list, pull all the values of the list and add them to the
        // surrounding list(children) as individual values
        // This is to add values of the list to the children[] list instead of adding the answers 
        // list to it that would form a nested list 
        ...(questions[questionIndex]['answers'] as List<Map<String, Object>>).map((answer) {
          // forward an anonymous function to answer which is only executed when the button is pressed
          // nb: answerquestion is a pointer to the function.
          // Created a function on the fly and only pass an address of the answerQuestion function 
          // to the answer widget
          return Answer(() => answerQuestion(answer['score']), answer['text']); 
        }).toList()
      ],
    );
  }
}
