import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  // Initialize property of type function
  // User final to enforce that the value of selectHandler will not
  // change after its initialization in the constructor
  // The pointer passed in the main function where answer widget is called is stored in
  // select handler property
  final Function selectHandler;

  // Second argument accepts a string of answers
  final String answerText;

  // Initialize constructor, pass selectHandler as a positional argument
  Answer(this.selectHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
            child: Text(answerText),
            // blue static property, that can be used without instantiating the class
            color: Colors.blue,
            textColor: Colors.white,

            // With parenthesis, answerquestion() gets exectuted when dart builds the button
            // Instead we awant to execute the button on pressed,
            // thus you need to pass a pointer to answer question on pressed to get the return value
            // Using the paranthesis will execute the function, instead of having it executed by the on pressed action
            // No parathesis passes the pointer to the function

            // The function that i want to assign to onPressed actually resides in the main dart file
            // because that is where the questionIndex is managed. The questions are also in the main file, 
            // which need to be there since the questions need to be passed to the question widget, which has 
            // no direct connection to the answer widget 
            // How do i get access to the _answerQuestion method(that resides in main) in the answer widget> 
            // By passing the pointer to the function that is passed from main to answer through its constructor
            onPressed: selectHandler,
          ),
    );
  }
}